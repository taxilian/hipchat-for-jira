package com.atlassian.labs.hipchat;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * A simple mapping class for HipCHat room. There are only needed properties.
 *
 * This class is API. Please make only additive changes to it until the next X.0 release
 */
public class Room
{
    private final Long roomId;
    private final String name;

    @JsonCreator
    public Room(@JsonProperty("room_id") Long roomId, @JsonProperty("name") String name) {
        this.roomId = roomId;
        this.name = name;
    }

    public Long getRoomId() {
        return roomId;
    }

    public String getName() {
        return name;
    }
}
