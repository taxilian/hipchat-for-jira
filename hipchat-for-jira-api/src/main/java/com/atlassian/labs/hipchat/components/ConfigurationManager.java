package com.atlassian.labs.hipchat.components;

/**
 * This interface is API. Please make only additive changes to it until the next X.0 release
 */
public interface ConfigurationManager {

    public String getHipChatApiToken();

    public void updateHipChatApiToken(String token);
}